#include "gtest/gtest.h"
#include <malloc.h>
#include <iostream>
#include <chrono>
#include <thread>

#define Mb 1024 * 1024

extern "C" {
    #include "parallel.h"
    #include "consistent.h"
}

int my_random(int max, int min) {
    int res = (random() % (max + 1 - min)) + min;
    return res;
}

void init_random(int* arr, int len) {
    for (int i = 0; i < len; ++i) {
        arr[i] = my_random(10000, -10000);
    }
}


class test : public ::testing::Test
{
protected:
	void SetUp()
	{
        srand(getpid());
        int size = 1 * Mb;
        len = size / sizeof(int);
        arr = (int*)malloc(len * sizeof(int));
        assert(arr != NULL);
        init_random(arr, len);
	}
	void TearDown()
	{
        free;
	}
    int len;
    int* arr;
};


TEST_F(test, parallel) {
   auto start = std::chrono::system_clock::now(); 
    { 
        find_max_diff_parallel(arr, len);
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "\nElapsed time: " << elapsed.count() << "s\n\n";
  ASSERT_EQ(1, 1);
}

TEST_F(test, consistent) {
   auto start = std::chrono::system_clock::now(); 
    { 
        find_max_diff_consistent(arr, 0, len);
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "\nElapsed time: " << elapsed.count() << "s\n\n";
  ASSERT_EQ(1, 1);
}