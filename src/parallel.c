#include <stdio.h>
#include <assert.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/wait.h>
#include "parallel.h"

#define Mb 1024 * 1024
#define MAX_LEN_INT 16
#define AMOUNT_DESC 2

int find_max_diff_consistent(int* arr, int start, int end) {
    int max_diff = 0;
    int tmp_diff = 0;
    for (int i = start; i < end - 1; ++i) {
        tmp_diff = arr[i + 1] - arr[i];
        if (max_diff < tmp_diff) max_diff = tmp_diff;
    }
    return max_diff;
}

int** alloc_pipes(int amount_process) {
    int** pipes = malloc(sizeof(int*) * amount_process);
    assert(pipes != NULL);
    for (int i = 0; i < amount_process; ++i) {
        pipes[i] = malloc(sizeof(int) * AMOUNT_DESC);
        assert(pipes[i] != NULL);
        int pipe_res = pipe(pipes[i]);
        assert(pipe_res == 0);
    }
    return pipes;
}

int find_max_int_in_pipes(int** pipes, int len) {
    char* max_values = malloc(MAX_LEN_INT);
    assert(max_values != NULL);
    int max = 0;
    int tmp = 0;
    for (int i = 0; i < len; ++i) {
        read(pipes[i][0], max_values, MAX_LEN_INT);
        tmp = atoi(max_values);
        if (max < tmp) max = tmp;
    }
    free(max_values);
    return max;
}

int find_max_diff_parallel(int* arr, int len) {
    int amount_cors = sysconf(_SC_NPROCESSORS_ONLN);
    int** pipes = alloc_pipes(amount_cors);
    int step = len / amount_cors;
    int pid = 1;
    pid_t* waiters = malloc(sizeof(pid_t) * amount_cors);
    assert(waiters != NULL);
    for (int i = 0; i < amount_cors; ++i) {
        pid = fork();
        int size_written = 0;
        char* buf = malloc(MAX_LEN_INT);
        switch (pid) {
            case -1:
                assert(pid != -1);
            case 0:
                close(pipes[i][0]);
                sprintf(buf,"%d",  find_max_diff_consistent(arr, step * i, step * (i + 1)));
                size_written = write(pipes[i][1], buf, MAX_LEN_INT);
                assert(size_written != 0);
                exit(1);
            default:
                close(pipes[i][1]);
                break;
        }
        waiters[i] = pid;
    }
    for (int i = 0; i < amount_cors; ++i) {
        wait(waiters[i]);
    }
    int max_diff = find_max_int_in_pipes(pipes, amount_cors);
    free(waiters);
    free(pipes);
    return max_diff;
}