#include "consistent.h"

int find_max_diff_consistent(int* arr, int start, int end) {
    int max_diff = 0;
    int tmp_diff = 0;
    for (int i = start; i < end - 1; ++i) {
        tmp_diff = arr[i + 1] - arr[i];
        if (max_diff < tmp_diff) max_diff = tmp_diff;
    }
    return max_diff;
}
