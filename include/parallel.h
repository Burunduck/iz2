#ifndef IZ2_PARALLEL_H
#define IZ2_PARALLEL_H

int find_max_diff_parallel(int* arr, int len);

#endif //IZ2_PARALLEL_H
