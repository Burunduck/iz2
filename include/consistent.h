#ifndef IZ2_CONSISTENT_H
#define IZ2_CONSISTENT_H

int find_max_diff_consistent(int* arr, int start, int end);

#endif //IZ2_CONSISTENT_H